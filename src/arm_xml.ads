with ARM_Contents, ARM_Output;
with Ada.Wide_Text_IO;

with Ada.Containers.Vectors;

package ARM_XML is

   type XML_Output_Type is new ARM_Output.Output_Type with private;

   procedure Create (Output_Object : in out XML_Output_Type);

   overriding procedure Close (Output_Object : in out XML_Output_Type);
   -- Close an Output_Object. No further output to the object is
   -- allowed after this call.

   overriding procedure Section
     (Output_Object : in out XML_Output_Type;
      Section_Title : in String;
      Section_Name  : in String);
   -- Start a new section. The title is Section_Title (this is
   -- intended for humans). The name is Section_Name (this is
   -- intended to be suitable to be a portion of a file name).

   overriding procedure Set_Columns
     (Output_Object     : in out XML_Output_Type;
      Number_of_Columns : in ARM_Output.Column_Count);
   -- Set the number of columns.
   -- Raises Not_Valid_Error if in a paragraph.

   overriding procedure Start_Paragraph
     (Output_Object  : in out XML_Output_Type;
      Style          : in ARM_Output.Paragraph_Style_Type;
      Indent         : in ARM_Output.Paragraph_Indent_Type;
      Number         : in String;
      No_Prefix      : in Boolean                       := False;
      Tab_Stops      : in ARM_Output.Tab_Info           := ARM_Output.NO_TABS;
      No_Breaks      : in Boolean                       := False;
      Keep_with_Next : in Boolean                       := False;
      Space_After    : in ARM_Output.Space_After_Type   := ARM_Output.Normal;
      Justification  : in ARM_Output.Justification_Type := ARM_Output.Default);
   -- Start a new paragraph. The style and indent of the paragraph is as
   -- specified. The (AA)RM paragraph number (which might include update
   -- and version numbers as well: [12.1/1]) is Number. If the format is
   -- a type with a prefix (bullets, hangining items), the prefix is
   -- omitted if No_Prefix is true. Tab_Stops defines the tab stops for
   -- the paragraph. If No_Breaks is True, we will try to avoid page breaks
   -- in the paragraph. If Keep_with_Next is true, we will try to avoid
   -- separating this paragraph and the next one. (These may have no
   -- effect in formats that don't have page breaks). Space_After
   -- specifies the amount of space following the paragraph. Justification
   -- specifies the text justification for the paragraph. Not_Valid_Error
   -- is raised if Tab_Stops /= NO_TABS for a hanging or bulleted format.

   overriding procedure End_Paragraph
     (Output_Object : in out XML_Output_Type);
   -- End a paragraph.

   overriding procedure Category_Header
     (Output_Object : in out XML_Output_Type;
      Header_Text   : String);
   -- Output a Category header (that is, "Legality Rules",
   -- "Dynamic Semantics", etc.)
   -- (Note: We did not use a enumeration here to insure that these
   -- headers are spelled the same in all output versions).
   -- Raises Not_Valid_Error if in a paragraph.

   overriding procedure Clause_Header
     (Output_Object : in out XML_Output_Type;
      Header_Text   : in String;
      Level         : in ARM_Contents.Level_Type;
      Clause_Number : in String;
      No_Page_Break : in Boolean := False);
   -- Output a Clause header. The level of the header is specified
   -- in Level. The Clause Number is as specified.
   -- These should appear in the table of contents.
   -- For hyperlinked formats, this should generate a link target.
   -- If No_Page_Break is True, suppress any page breaks.
   -- Raises Not_Valid_Error if in a paragraph.

   overriding procedure Revised_Clause_Header
     (Output_Object   : in out XML_Output_Type;
      New_Header_Text : in String;
      Old_Header_Text : in String;
      Level           : in ARM_Contents.Level_Type;
      Clause_Number   : in String;
      Version         : in ARM_Contents.Change_Version_Type;
      Old_Version     : in ARM_Contents.Change_Version_Type;
      No_Page_Break   : in Boolean := False);
   -- Output a revised clause header. Both the original and new text will
   -- be output. The level of the header is specified in Level. The Clause
   -- Number is as specified.
   -- These should appear in the table of contents.
   -- For hyperlinked formats, this should generate a link target.
   -- If No_Page_Break is True, suppress any page breaks.
   -- Raises Not_Valid_Error if in a paragraph.

   overriding procedure TOC_Marker
     (Output_Object : in out XML_Output_Type;
      For_Start     : in Boolean);
   -- Mark the start (if For_Start is True) or end (if For_Start is
   -- False) of the table of contents data. Output objects that
   -- auto-generate the table of contents can use this to do needed
   -- actions.

   overriding procedure New_Page
     (Output_Object : in out XML_Output_Type;
      Kind          : ARM_Output.Page_Kind_Type := ARM_Output.Any_Page);
   -- Output a page break.
   -- Note that this has no effect on non-printing formats.
   -- Any_Page breaks to the top of the next page (whatever it is);
   -- Odd_Page_Only breaks to the top of the odd-numbered page;
   -- Soft_Page allows a page break but does not force one (use in
   -- "No_Breaks" paragraphs.)
   -- Raises Not_Valid_Error if in a paragraph if Kind = Any_Page or
   -- Odd_Page, and if not in a paragraph if Kind = Soft_Page.

   overriding procedure New_Column (Output_Object : in out XML_Output_Type);
   -- Output a column break.
   -- Raises Not_Valid_Error if in a paragraph, or if the number of
   -- columns is 1.

   overriding procedure Separator_Line
     (Output_Object : in out XML_Output_Type;
      Is_Thin       : Boolean := True);
   -- Output a separator line. It is thin if "Is_Thin" is true.
   -- Raises Not_Valid_Error if in a paragraph.

   overriding procedure Start_Table
     (Output_Object      : in out XML_Output_Type;
      Columns            : in ARM_Output.Column_Count;
      First_Column_Width : in ARM_Output.Column_Count;
      Last_Column_Width  : in ARM_Output.Column_Count;
      Alignment          : in ARM_Output.Column_Text_Alignment;
      No_Page_Break      : in Boolean;
      Has_Border         : in Boolean;
      Small_Text_Size    : in Boolean;
      Header_Kind        : in ARM_Output.Header_Kind_Type);
   -- Starts a table. The number of columns is Columns; the first
   -- column has First_Column_Width times the normal column width, and
   -- the last column has Last_Column_Width times the normal column width.
   -- Alignment is the horizontal text alignment within the columns.
   -- No_Page_Break should be True to keep the table intact on a single
   -- page; False to allow it to be split across pages.
   -- Has_Border should be true if a border is desired, false otherwise.
   -- Small_Text_Size means that the contents will have the AARM size;
   -- otherwise it will have the normal size.
   -- Header_Kind determines whether the table has headers.
   -- This command starts a paragraph; the entire table is a single
   -- paragraph. Text will be considered part of the caption until the
   -- next table marker call.
   -- Raises Not_Valid_Error if in a paragraph.

   overriding procedure Table_Marker
     (Output_Object : in out XML_Output_Type;
      Marker        : in ARM_Output.Table_Marker_Type);
   -- Marks the end of an entity in a table.
   -- If Marker is End_Caption, the table caption ends and the
   --	future text is part of the table header.
   -- If Marker is End_Header, the table header ends and the
   --	future text is part of the table body.
   -- If Marker is End_Row, a row in the table is completed, and another
   --	row started.
   -- If Marker is End_Row_Next_Is_Last, a row in the table is completed,
   --	and another row started. That row is the last row in the table.
   -- If Marker is End_Item, an item in the table header or body is ended,
   --	and another started.
   -- If Marker is End_Table, the entire table is finished.
   -- Raises Not_Valid_Error if not in a table.

   -- Text output: These are only allowed after a Start_Paragraph and
   -- before any End_Paragraph. Raises Not_Valid_Error if not allowed.

   overriding procedure Ordinary_Text
     (Output_Object : in out XML_Output_Type;
      Text          : in String);
   -- Output ordinary text.
   -- The text must end at a word break, never in the middle of a word.

   overriding procedure Ordinary_Character
     (Output_Object : in out XML_Output_Type;
      Char          : in Character);
   -- Output an ordinary (Latin-1) character.
   -- Spaces will be used to break lines as needed.

   overriding procedure Hard_Space (Output_Object : in out XML_Output_Type);
   -- Output a hard space. No line break should happen at a hard space.

   overriding procedure Line_Break (Output_Object : in out XML_Output_Type);
   -- Output a line break. This does not start a new paragraph.
   -- This corresponds to a "<BR>" in HTML.

   overriding procedure Index_Line_Break
     (Output_Object        : in out XML_Output_Type;
      Clear_Keep_with_Next : in Boolean);
   -- Output a line break for the index. This does not start a new
   -- paragraph in terms of spacing. This corresponds to a "<BR>"
   -- in HTML. If Clear_Keep_with_Next is true, insure that the next
   -- line does not require the following line to stay with it.
   -- Raises Not_Valid_Error if the paragraph is not in the index format.

   overriding procedure Soft_Line_Break
     (Output_Object : in out XML_Output_Type);
   -- Output a soft line break. This is a place (in the middle of a
   -- "word") that we allow a line break. It is usually used after
   -- underscores in long non-terminals.

   overriding procedure Soft_Hyphen_Break
     (Output_Object : in out XML_Output_Type);
   -- Output a soft line break, with a hyphen. This is a place (in the middle
   --of
   -- a "word") that we allow a line break. If the line break is used,
   -- a hyphen will be added to the text.

   overriding procedure Tab (Output_Object : in out XML_Output_Type);
   -- Output a tab, inserting space up to the next tab stop.
   -- Raises Not_Valid_Error if the paragraph was created with
   -- Tab_Stops = ARM_Output.NO_TABS.

   overriding procedure Special_Character
     (Output_Object : in out XML_Output_Type;
      Char          : in ARM_Output.Special_Character_Type);
   -- Output an special character.

   overriding procedure Unicode_Character
     (Output_Object : in out XML_Output_Type;
      Char          : in ARM_Output.Unicode_Type);
   -- Output a Unicode character, with code position Char.

   overriding procedure End_Hang_Item
     (Output_Object : in out XML_Output_Type);
   -- Marks the end of a hanging item. Call only once per paragraph.
   -- Raises Not_Valid_Error if the paragraph style is not in
   -- Text_Prefixed_Style_Subtype, or if this has already been
   -- called for the current paragraph, or if the paragraph was started
   -- with No_Prefix = True.

   overriding procedure Text_Format
     (Output_Object : in out XML_Output_Type;
      Format        : in ARM_Output.Format_Type);
   -- Change the text format so that all of the properties are as specified.
   -- Note: Changes to these properties ought be stack-like; that is,
   -- Bold on, Italic on, Italic off, Bold off is OK; Bold on, Italic on,
   -- Bold off, Italic off should be avoided (as separate commands).

   overriding procedure Clause_Reference
     (Output_Object : in out XML_Output_Type;
      Text          : in String;
      Clause_Number : in String);
   -- Generate a reference to a clause in the standard. The text of
   -- the reference is "Text", and the number of the clause is
   -- Clause_Number. For hyperlinked formats, this should generate
   -- a link; for other formats, the text alone is generated.

   overriding procedure Index_Target
     (Output_Object : in out XML_Output_Type;
      Index_Key     : in Natural);
   -- Generate a index target. This marks the location where an index
   -- reference occurs. Index_Key names the index item involved.
   -- For hyperlinked formats, this should generate a link target;
   -- for other formats, nothing is generated.

   overriding procedure Index_Reference
     (Output_Object : in out XML_Output_Type;
      Text          : in String;
      Index_Key     : in Natural;
      Clause_Number : in String);
   -- Generate a reference to an index target in the standard. The text
   -- of the reference is "Text", and Index_Key and Clause_Number denotes
   -- the target. For hyperlinked formats, this should generate
   -- a link; for other formats, the text alone is generated.

   overriding procedure DR_Reference
     (Output_Object : in out XML_Output_Type;
      Text          : in String;
      DR_Number     : in String);
   -- Generate a reference to a DR from the standard. The text
   -- of the reference is "Text", and DR_Number denotes
   -- the target. For hyperlinked formats, this should generate
   -- a link; for other formats, the text alone is generated.

   overriding procedure AI_Reference
     (Output_Object : in out XML_Output_Type;
      Text          : in String;
      AI_Number     : in String);
   -- Generate a reference to an AI from the standard. The text
   -- of the reference is "Text", and AI_Number denotes
   -- the target (in unfolded format). For hyperlinked formats, this
   -- should generate a link; for other formats, the text alone is
   -- generated.

   overriding procedure Local_Target
     (Output_Object : in out XML_Output_Type;
      Text          : in String;
      Target        : in String);
   -- Generate a local target. This marks the potential target of local
   -- links identified by "Target". Text is the text of the target.
   -- For hyperlinked formats, this should generate a link target;
   -- for other formats, only the text is generated.

   overriding procedure Local_Link
     (Output_Object : in out XML_Output_Type;
      Text          : in String;
      Target        : in String;
      Clause_Number : in String);
   -- Generate a local link to the target and clause given.
   -- Text is the text of the link.
   -- For hyperlinked formats, this should generate a link;
   -- for other formats, only the text is generated.

   overriding procedure Local_Link_Start
     (Output_Object : in out XML_Output_Type;
      Target        : in String;
      Clause_Number : in String);
   -- Generate a local link to the target and clause given.
   -- The link will surround text until Local_Link_End is called.
   -- Local_Link_End must be called before this routine can be used again.
   -- For hyperlinked formats, this should generate a link;
   -- for other formats, only the text is generated.

   overriding procedure Local_Link_End
     (Output_Object : in out XML_Output_Type;
      Target        : in String;
      Clause_Number : in String);
   -- End a local link for the target and clause given.
   -- This must be in the same paragraph as the Local_Link_Start.
   -- For hyperlinked formats, this should generate a link;
   -- for other formats, only the text is generated.

   overriding procedure URL_Link
     (Output_Object : in out XML_Output_Type;
      Text          : in String;
      URL           : in String);
   -- Generate a link to the URL given.
   -- Text is the text of the link.
   -- For hyperlinked formats, this should generate a link;
   -- for other formats, only the text is generated.

   overriding procedure Picture
     (Output_Object : in out XML_Output_Type;
      Name          : in String;
      Descr         : in String;
      Alignment     : in ARM_Output.Picture_Alignment;
      Height, Width : in Natural;
      Border        : in ARM_Output.Border_Kind);
   -- Generate a picture.
   -- Name is the (simple) file name of the picture; Descr is a
   -- descriptive name for the picture (it will appear in some web
   -- browsers).
   -- We assume that it is a .PNG or .JPG and that it will be present
   -- in the same directory as the output files.
   -- Alignment specifies the picture alignment.
   -- Height and Width specify the picture size in pixels.
   -- Border specifies the kind of border.

private

   use type ARM_Output.Format_Type;

   type Format_Tags is (Location, Added_Version, Version, Change,
                        Color, Size, Font, Italic, Bold);

   package Format_Vectors is new Ada.Containers.Vectors (Index_Type => Natural,
                                                         Element_Type => Format_Tags);
   -- Used to remember the format-tags that have been opened.

   use type ARM_Contents.Level_Type;

   package Level_Vectors is new Ada.Containers.Vectors (Index_Type => Natural,
                                                        Element_Type => ARM_Contents.Level_Type);
   -- Used to remember the section- and clause-tags that have been opened.

   Prefix : String := "../output/"; -- Needs to be moved somewhere else.

   type XML_Output_Type is new ARM_Output.Output_Type with record
      Output_File     : Ada.Wide_Text_IO.File_Type;
      Is_Valid        : Boolean; -- For Example: has not been closed, no errors etc.
      Format          : Format_Vectors.Vector := Format_Vectors.Empty_Vector;
      Level           : Level_Vectors.Vector := Level_Vectors.Empty_Vector;
      Open_Paragraph  : Boolean := False;
      Open_Category   : Boolean := False;
      Current_Format  : ARM_Output.Format_Type;
      Table_Last_Row  : Boolean := False; -- After this row has been closed, no new row needs to be opened
      Table_In_Header : Boolean := False;
      Columns_Is_Open : Boolean := False;
      Columns_Count   : ARM_Output.Column_Count := 1;
   end record;
end ARM_XML;
