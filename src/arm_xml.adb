with Ada.Characters.Handling;
with Ada.Directories;
with Ada.Strings.Fixed;
with Ada.Strings.Maps;
with Ada.Wide_Text_IO;

-- Debug
with Ada.Text_IO;

package body ARM_XML is

   -- Rename the Ada.Wide_Text_IO.Open function to make it create files
   -- in UTF-8 mode.

   procedure Create
     (File : in out Ada.Wide_Text_IO.File_Type;
      Mode : in Ada.Wide_Text_IO.File_Mode := Ada.Wide_Text_IO.Out_File;
      Name : in String                     := "";
      Form : in String                     := "WCEM=8") renames
     Ada.Wide_Text_IO.Create;

   -- Some helper functions

   function WS (Item : in String) return Wide_String renames
     Ada.Characters.Handling.To_Wide_String;

   function WC (Item : in Character) return Wide_Character renames
     Ada.Characters.Handling.To_Wide_Character;

   procedure Is_Valid (Output_Object : in XML_Output_Type) is
   -- Assert that the Output_Object is valid.
   -- Raises Not_Valid_Error otherwise.
   begin
      if not Ada.Wide_Text_IO.Is_Open (Output_Object.Output_File) then
         Ada.Text_IO.Put_Line ("Output file is not open.");
      end if;

      if not Output_Object.Is_Valid then
         Ada.Text_IO.Put_Line ("Output object is invalid.");
      end if;

      if not Output_Object.Is_Valid
        or else not Ada.Wide_Text_IO.Is_Open (Output_Object.Output_File)
      then
         raise ARM_Output.Not_Valid_Error;
      end if;
   end Is_Valid;

   procedure Output
     (Output_Object : in XML_Output_Type;
      Item          : in Wide_String)
   is
   begin
      Is_Valid (Output_Object);
      Ada.Wide_Text_IO.Put (Output_Object.Output_File, Item);
   end Output;

   procedure Output
     (Output_Object : in XML_Output_Type;
      Item          : in Wide_Character)
   is
   begin
      Output (Output_Object, Item & "");
   end Output;

   procedure Output_Line
     (Output_Object : in XML_Output_Type;
      Item          : in Wide_String)
   is
   begin
      Is_Valid (Output_Object);
      Ada.Wide_Text_IO.Put_Line (Output_Object.Output_File, Item);
   end Output_Line;

   ------------

   procedure Close_Last_Format (Output_Object : in out XML_Output_Type) is
      Format : Format_Tags := Output_Object.Format.Last_Element;
   begin
      Is_Valid (Output_Object);

      if Output_Object.Format.Is_Empty then
         -- Do nothing.
         return;
      end if;

      Format := Output_Object.Format.Last_Element;

      case Format is
         when Bold =>
            Output_Line (Output_Object, "</bold>");
         when Italic =>
            Output_Line (Output_Object, "</italic>");
         when Font =>
            Output_Line (Output_Object, "</font>");
         when Size =>
            Output_Line (Output_Object, "</size>");
         when Color =>
            Output_Line (Output_Object, "</color>");
         when Change =>
            Output_Line (Output_Object, "</change>");
         when Version =>
            Output_Line (Output_Object, "</version>");
         when Added_Version =>
            Output_Line (Output_Object, "</added_version>");
         when Location =>
            Output_Line (Output_Object, "</location>");
      end case;
      Output_Object.Format.Delete_Last;
   end Close_Last_Format;

   procedure Close_Last_Level (Output_Object : in out XML_Output_Type) is
      Level : ARM_Contents.Level_Type;
   begin
      Is_Valid (Output_Object);

      if Output_Object.Level.Is_Empty then
         -- Do nothing;
         return;
      end if;

      Level := Output_Object.Level.Last_Element;

      case Level is
         when ARM_Contents.Section =>
            Output_Line (Output_Object, "</section>");
         when ARM_Contents.Unnumbered_Section =>
            Output_Line (Output_Object, "</unnumbered_section>");
         when ARM_Contents.Plain_Annex =>
            Output_Line (Output_Object, "</plain_annex>");
         when ARM_Contents.Normative_Annex =>
            Output_Line (Output_Object, "</normative_annex>");
         when ARM_Contents.Informative_Annex =>
            Output_Line (Output_Object, "</informative_annex>");
         when ARM_Contents.Clause =>
            Output_Line (Output_Object, "</clause>");
         when ARM_Contents.Subclause =>
            Output_Line (Output_Object, "</subclause>");
         when ARM_Contents.Subsubclause =>
            Output_Line (Output_Object, "</subsubclause>");
         when ARM_Contents.Dead_Clause =>
            Output_Line (Output_Object, "</dead_clause>");
      end case;
      Output_Object.Level.Delete_Last;
   end Close_Last_Level;

   ------------

   procedure Close_Format_Until
     (Output_Object : in out XML_Output_Type;
      Item          : in Format_Tags)
   is
   begin
      while not Output_Object.Format.Is_Empty
        and then Output_Object.Format.Last_Element /= Item
      loop
         Close_Last_Format (Output_Object);
      end loop;
   end Close_Format_Until;

   ------------

   procedure Close_Level_Until
     (Output_Object : in out XML_Output_Type;
      Item          : in ARM_Contents.Level_Type)
   is
      use type ARM_Contents.Level_Type;
   begin
      while not Output_Object.Level.Is_Empty
        and then Output_Object.Level.Last_Element /= Item
      loop
         Close_Last_Level (Output_Object);
      end loop;
   end Close_Level_Until;

   ------------

   procedure Close_All_Format_Tags (Output_Object : in out XML_Output_Type) is
   begin
      while not Output_Object.Format.Is_Empty loop
         Close_Last_Format (Output_Object);
      end loop;
   end Close_All_Format_Tags;

   procedure Close_All_Level_Tags (Output_Object : in out XML_Output_Type) is
   begin
      while not Output_Object.Level.Is_Empty loop
         Close_Last_Level (Output_Object);
      end loop;
   end Close_All_Level_Tags;

   ------------

   function Level_Is_Open
     (Output_Object : in XML_Output_Type;
      Level         : in ARM_Contents.Level_Type)
      return          Boolean
   is
   begin
      return Output_Object.Level.Contains (Level);
   end Level_Is_Open;

   ------------
   -- Create --
   ------------

   procedure Create (Output_Object : in out XML_Output_Type) is
   begin
      -- For now there is nothing to do here.
      Output_Object.Is_Valid := True;
   end Create;

   -----------
   -- Close --
   -----------

   procedure Close (Output_Object : in out XML_Output_Type) is
   begin
      Is_Valid (Output_Object);
      -- "Unwind" all tags.
      Close_All_Format_Tags (Output_Object);
      Close_All_Level_Tags (Output_Object);
      Ada.Wide_Text_IO.Close (Output_Object.Output_File);

      Output_Object.Is_Valid := False; -- No further output allowed.
      Ada.Text_IO.Put_Line ("The file was closed.");
   end Close;

   -------------
   -- Section --
   -------------

   procedure Section
     (Output_Object : in out XML_Output_Type;
      Section_Title : in String;
      Section_Name  : in String)
   is
   -- Create one file per section.
   begin
      if Ada.Wide_Text_IO.Is_Open (Output_Object.Output_File) then
         Close_Level_Until (Output_Object, ARM_Contents.Section);
         -- Close the category (if one has been opened). TODO : put it on the
         -- level stack.
         if Output_Object.Open_Category then
            Output_Line (Output_Object, "</content>");
            Output_Line (Output_Object, "</category>");
            Output_Object.Open_Category := False;
         end if;
         -- Close the section and then close the file.
         Output (Output_Object, "</section>");
         Ada.Wide_Text_IO.Close (Output_Object.Output_File);
      end if;
      -- Build the filename
      declare
      -- TODO: Find out wether it is the RM or Annotated RM or whatever and
      --change the prefix accordingly.
      -- TODO: Decide on wether to omit leading zeros in the Section_Name.
         Filename : String := Prefix & "RM-" & Section_Name & ".xml";
      begin
         -- Check if the file for that section already exists,
         -- if so our Output_Object is rendered invalid.
         if Ada.Directories.Exists (Filename) then
            Output_Object.Is_Valid := False;
            Ada.Text_IO.Put_Line
              ("The file '" & Filename & "' already exists.");
         else -- Otherwise create the new File.
            Create (File => Output_Object.Output_File, Name => Filename);
            -- Write the header and open the section tag.
            Output_Line
              (Output_Object,
               "<?xml version=""1.0"" encoding=""UTF-8"" ?>");
            Output_Line (Output_Object, "<section>");
            Output_Object.Level.Append (ARM_Contents.Section);
         end if;
      end;
   end Section;

   -----------------
   -- Set_Columns --
   -----------------

   procedure Set_Columns
     (Output_Object     : in out XML_Output_Type;
      Number_of_Columns : in ARM_Output.Column_Count)
   is
   begin
      Is_Valid (Output_Object);

      if Output_Object.Open_Paragraph then
         raise ARM_Output.Not_Valid_Error; -- see spec file.
      end if;

      if Number_of_Columns = 1 and Output_Object.Columns_Is_Open then
         Output_Line (Output_Object, "</col>");
         Output_Line (Output_Object, "</columns>");
         Output_Object.Columns_Is_Open := False;
         Output_Object.Columns_Count   := 1;
      elsif Number_of_Columns /= Output_Object.Columns_Count then
         if Output_Object.Columns_Is_Open then
            Output_Line (Output_Object, "</col>");
            Output_Line (Output_Object, "</columns>");
         end if;
         declare
            use Ada.Strings;

            Col_Num : String :=
               Fixed.Trim
                 (ARM_Output.Column_Count'Image (Number_of_Columns),
                  Left);
         begin

            Output_Line
              (Output_Object,
               "<columns num=""" & WS (Col_Num) & """>");
         end;
         Output_Line (Output_Object, "<col>");

         Output_Object.Columns_Is_Open := True;
         Output_Object.Columns_Count   := Number_of_Columns;
      end if;
   end Set_Columns;

   ---------------------
   -- Start_Paragraph --
   ---------------------

   procedure Start_Paragraph
     (Output_Object  : in out XML_Output_Type;
      Style          : in ARM_Output.Paragraph_Style_Type;
      Indent         : in ARM_Output.Paragraph_Indent_Type;
      Number         : in String;
      No_Prefix      : in Boolean                       := False;
      Tab_Stops      : in ARM_Output.Tab_Info           := ARM_Output.NO_TABS;
      No_Breaks      : in Boolean                       := False;
      Keep_with_Next : in Boolean                       := False;
      Space_After    : in ARM_Output.Space_After_Type   := ARM_Output.Normal;
      Justification  : in ARM_Output.Justification_Type := ARM_Output.Default)
   is
   begin
      Is_Valid (Output_Object);
      if not Output_Object.Open_Paragraph then
         declare
            use Ada.Characters.Handling;
            use Ada.Strings;

            Style_Attr  : String :=
               To_Lower (ARM_Output.Paragraph_Style_Type'Image (Style));
            Indent_Attr : String :=
               Fixed.Trim
                 (ARM_Output.Paragraph_Indent_Type'Image (Indent),
                  Left);
         begin --TODO : Deal with all the attributes
            Output
              (Output_Object,
               "<paragraph style=""" &
               WS (Style_Attr) &
               """ indent=""" &
               WS (Indent_Attr));
            if Number /= "" then
               Output (Output_Object, """ number=""" & WS (Number));
            end if;
            Output (Output_Object, """>");
         end;
         Output_Object.Open_Paragraph := True;
      else
         Output_Object.Is_Valid := False;
      end if;
   end Start_Paragraph;

   -------------------
   -- End_Paragraph --
   -------------------

   procedure End_Paragraph (Output_Object : in out XML_Output_Type) is
   begin
      Is_Valid (Output_Object);
      if Output_Object.Open_Paragraph then
         Close_All_Format_Tags (Output_Object);
         Output_Line (Output_Object, ""); -- newline
         Output_Line (Output_Object, "</paragraph>");
         Output_Object.Open_Paragraph := False;
      else
         --  Output_Object.Is_Valid := False;
         Ada.Text_IO.Put_Line ("Wrong paragraph order.");
      end if;
   end End_Paragraph;

   ---------------------
   -- Category_Header --
   ---------------------

   procedure Category_Header
     (Output_Object : in out XML_Output_Type;
      Header_Text   : String)
   is
   begin
      Is_Valid (Output_Object);

      Close_Level_Until (Output_Object, ARM_Contents.Section);

      if Output_Object.Open_Category then
         Output_Line (Output_Object, "</category>");  -- Hopefully
                                                      --all tags have
                                                      --been closed.
                                                      --Good Luck!
                                                      --TODO
      end if;

      Output_Line (Output_Object, "<category>");
      Output_Line
        (Output_Object,
         "<category_header>" & WS (Header_Text) & "</category_header>");

      Output_Object.Open_Category := True;
   end Category_Header;

   -------------------
   -- Clause_Header --
   -------------------

   procedure Clause_Header
     (Output_Object : in out XML_Output_Type;
      Header_Text   : in String;
      Level         : in ARM_Contents.Level_Type;
      Clause_Number : in String;
      No_Page_Break : in Boolean := False) -- Todo: integrate nopagebreak
   is
   begin
      Is_Valid (Output_Object);

      case Level is
         when ARM_Contents.Section =>
            Output (Output_Object, "<clause_header level=""section"">");
         when ARM_Contents.Unnumbered_Section =>
            -- Apparently, when a new unnumbered section is opened the
            --previous has to be closed. But only when an Unnumbered_Section
            --was opened before!
            if Level_Is_Open
                 (Output_Object,
                  ARM_Contents.Unnumbered_Section)
            then
               Close_Level_Until
                 (Output_Object,
                  ARM_Contents.Unnumbered_Section);   -- Close the levels
                                                      --until the
                                                      --Unnumbered_Section
                                                      --level has been reached.
               Close_Last_Level (Output_Object);   -- Close the
                                                   --Unnumbered_section level
            end if;
            Output_Line (Output_Object, "<unnumbered_section>");
            Output
              (Output_Object,
               "<clause_header level=""unnumbered_section"">");
            Output_Object.Level.Append (ARM_Contents.Unnumbered_Section);
         when ARM_Contents.Plain_Annex =>
            Output_Line (Output_Object, "<plain_annex>");
            Output (Output_Object, "<clause_header level=""plain_annex"">");
            Output_Object.Level.Append (ARM_Contents.Plain_Annex);
         when ARM_Contents.Normative_Annex =>
            Output_Line (Output_Object, "<normative_annex>");
            Output
              (Output_Object,
               "<clause_header level=""normative_annex"">");
            Output_Object.Level.Append (ARM_Contents.Normative_Annex);
         when ARM_Contents.Informative_Annex =>
            Output_Line (Output_Object, "<informative_annex>");
            Output
              (Output_Object,
               "<clause_header level=""informative_annex"">");
            Output_Object.Level.Append (ARM_Contents.Informative_Annex);
         when ARM_Contents.Clause =>
            Output_Line (Output_Object, "<clause>");
            Output (Output_Object, "<clause_header level=""clause"">");
            Output_Object.Level.Append (ARM_Contents.Clause);
         when ARM_Contents.Subclause =>
            Output_Line (Output_Object, "<subclause>");
            Output (Output_Object, "<clause_header level=""subclause"">");
            Output_Object.Level.Append (ARM_Contents.Subclause);
         when ARM_Contents.Subsubclause =>
            Output_Line (Output_Object, "<subsubclause>");
            Output (Output_Object, "<clause_header level=""subsubclause"">");
            Output_Object.Level.Append (ARM_Contents.Subsubclause);
         when ARM_Contents.Dead_Clause =>
            Output_Line (Output_Object, "<dead_clause>");
            Output (Output_Object, "<clause_header level=""dead_clause"">");
            Output_Object.Level.Append (ARM_Contents.Dead_Clause);
      end case;
      Output_Line (Output_Object, WS (Header_Text) & "</clause_header>");
   end Clause_Header;

   ---------------------------
   -- Revised_Clause_Header --
   ---------------------------

   procedure Revised_Clause_Header
     (Output_Object   : in out XML_Output_Type;
      New_Header_Text : in String;
      Old_Header_Text : in String;
      Level           : in ARM_Contents.Level_Type;
      Clause_Number   : in String;
      Version         : in ARM_Contents.Change_Version_Type;
      Old_Version     : in ARM_Contents.Change_Version_Type;
      No_Page_Break   : in Boolean := False)
   is
   begin
      -- TODO: complete the stub.
      null;
   end Revised_Clause_Header;

   ----------------
   -- TOC_Marker --
   ----------------

   procedure TOC_Marker
     (Output_Object : in out XML_Output_Type;
      For_Start     : in Boolean)
   is
   begin
      Is_Valid (Output_Object);
      if For_Start then
         Output_Line (Output_Object, "<table_of_contents>");
      else
         Output_Line (Output_Object, "</table_of_contents>");
      end if;
   end TOC_Marker;

   --------------
   -- New_Page --
   --------------

   procedure New_Page
     (Output_Object : in out XML_Output_Type;
      Kind          : ARM_Output.Page_Kind_Type := ARM_Output.Any_Page)
   is
   begin
      Is_Valid (Output_Object);
      Output_Line (Output_Object, "<page_break />");
   end New_Page;

   ----------------
   -- New_Column --
   ----------------

   procedure New_Column (Output_Object : in out XML_Output_Type) is
   begin

      Is_Valid (Output_Object);

      Output_Line (Output_Object, "</col>");
      Output_Line (Output_Object, "<col>");
   end New_Column;

   --------------------
   -- Separator_Line --
   --------------------

   procedure Separator_Line
     (Output_Object : in out XML_Output_Type;
      Is_Thin       : Boolean := True)
   is
   begin
      Is_Valid (Output_Object);
      if Is_Thin then
         Output_Line (Output_Object, "<separator_line thin=""yes"" />");
      else
         Output_Line (Output_Object, "<separator_line thin=""no"" />");
      end if;
   end Separator_Line;

   -----------------
   -- Start_Table --
   -----------------

   procedure Start_Table
     (Output_Object      : in out XML_Output_Type;
      Columns            : in ARM_Output.Column_Count;
      First_Column_Width : in ARM_Output.Column_Count;
      Last_Column_Width  : in ARM_Output.Column_Count;
      Alignment          : in ARM_Output.Column_Text_Alignment;
      No_Page_Break      : in Boolean;
      Has_Border         : in Boolean;
      Small_Text_Size    : in Boolean;
      Header_Kind        : in ARM_Output.Header_Kind_Type)
   is
   begin
      Is_Valid (Output_Object);

      Output_Line (Output_Object, "<table>");

      case Header_Kind is
         when ARM_Output.Both_Caption_and_Header =>
            Output (Output_Object, "<caption>");
         when ARM_Output.Header_Only =>
            Output (Output_Object, "<tr><th>");
            Output_Object.Table_In_Header := True;
         when ARM_Output.No_Headers =>
            null; -- Nothing to do here!
      end case;

      Output_Object.Table_Last_Row := False;
   end Start_Table;

   ------------------
   -- Table_Marker --
   ------------------

   procedure Table_Marker
     (Output_Object : in out XML_Output_Type;
      Marker        : in ARM_Output.Table_Marker_Type)
   is
   begin
      Is_Valid (Output_Object);

      case Marker is
         when ARM_Output.End_Caption =>
            Output_Line (Output_Object, "</caption>");
            Output (Output_Object, "<tr><th>");
         when ARM_Output.End_Item =>
            if Output_Object.Table_In_Header then
               Output_Line (Output_Object, "</th>");
               Output (Output_Object, "<th>");
            else
               Output_Line (Output_Object, "</td>");
               Output_Line (Output_Object, "<td>");
            end if;
         when ARM_Output.End_Header =>
            Output_Line (Output_Object, "</th></tr>");
            Output (Output_Object, "<tr><td>");
            Output_Object.Table_In_Header := False;
         when ARM_Output.End_Row =>
            Output_Line (Output_Object, "</td></tr>");
            if not Output_Object.Table_Last_Row then
               Output (Output_Object, "<tr><td>");
            end if;
         when ARM_Output.End_Row_Next_Is_Last =>
            Output_Object.Table_Last_Row := True;
            Output_Line (Output_Object, "</td></tr>");
            Output (Output_Object, "<tr><td>");
         when ARM_Output.End_Table =>
            Output_Line (Output_Object, "</table>");
      end case;
   end Table_Marker;

   -------------------
   -- Ordinary_Text --
   -------------------

   procedure Ordinary_Text
     (Output_Object : in out XML_Output_Type;
      Text          : in String)
   is
   begin
      Is_Valid (Output_Object);

      for I in Text'Range loop
         Ordinary_Character (Output_Object, Text (I));
      end loop;
   end Ordinary_Text;

   ------------------------
   -- Ordinary_Character --
   ------------------------

   procedure Ordinary_Character
     (Output_Object : in out XML_Output_Type;
      Char          : in Character)
   is
   begin
      Is_Valid (Output_Object);

      case Char is
         when '&' =>
            Output (Output_Object, "&amp;");
         when '<' =>
            Output (Output_Object, "&lt;");
         when '>' =>
            Output (Output_Object, "&gt;");
         when others =>
            Output (Output_Object, WC (Char));
      end case;
   end Ordinary_Character;

   ----------------
   -- Hard_Space --
   ----------------

   procedure Hard_Space (Output_Object : in out XML_Output_Type) is
   begin
      Is_Valid (Output_Object);

      Output (Output_Object, Wide_Character'Val (16#00a0#));
   end Hard_Space;

   ----------------
   -- Line_Break --
   ----------------

   procedure Line_Break (Output_Object : in out XML_Output_Type) is
   begin
      Is_Valid (Output_Object);
      Output_Line (Output_Object, "<br />");
   end Line_Break;

   ----------------------
   -- Index_Line_Break --
   ----------------------

   procedure Index_Line_Break
     (Output_Object        : in out XML_Output_Type;
      Clear_Keep_with_Next : in Boolean)
   is
   begin
      -- TODO : Find out what to do here. Until fixed a normal line_break is
      --written.
      Line_Break (Output_Object);
   end Index_Line_Break;

   ---------------------
   -- Soft_Line_Break --
   ---------------------

   procedure Soft_Line_Break (Output_Object : in out XML_Output_Type) is
   begin
      Is_Valid (Output_Object);
      Output (Output_Object, "<soft_line_break />");
   end Soft_Line_Break;

   -----------------------
   -- Soft_Hyphen_Break --
   -----------------------

   procedure Soft_Hyphen_Break (Output_Object : in out XML_Output_Type) is
   begin
      Is_Valid (Output_Object);
      Output (Output_Object, "<soft_hyphen_break />");
   end Soft_Hyphen_Break;

   ---------
   -- Tab --
   ---------

   procedure Tab (Output_Object : in out XML_Output_Type) is
   begin
      Is_Valid (Output_Object);
      Output (Output_Object, "<tab />");
   end Tab;

   -----------------------
   -- Special_Character --
   -----------------------

   procedure Special_Character
     (Output_Object : in out XML_Output_Type;
      Char          : in ARM_Output.Special_Character_Type)
   is
   begin
      Is_Valid (Output_Object);
      case Char is
      when ARM_Output.EM_Dash => -- EM (very long) dash.
         Output (Output_Object, Wide_Character'Val (16#2014#));
      when ARM_Output.EN_Dash => -- EN (long) dash
         Output (Output_Object, Wide_Character'Val (16#2013#));
      when ARM_Output.GEQ => -- Greater than or equal symbol.
         Output (Output_Object, Wide_Character'Val (16#2265#));
      when ARM_Output.LEQ => -- Less than or equal symbol.
         Output (Output_Object, Wide_Character'Val (16#2264#));
      when ARM_Output.NEQ => -- Not equal symbol.
         Output (Output_Object, Wide_Character'Val (16#2260#));
      when ARM_Output.PI =>  -- PI.
         Output (Output_Object, Wide_Character'Val (16#03c0#));
      when ARM_Output.Left_Ceiling => -- Left half of ceiling.
         Output (Output_Object, Wide_Character'Val (16#2308#));
      when ARM_Output.Right_Ceiling => -- Right half of ceiling.
         Output (Output_Object, Wide_Character'Val (16#2309#));
      when ARM_Output.Left_Floor => -- Left half of floor.
         Output (Output_Object, Wide_Character'Val (16#230a#));
      when ARM_Output.Right_Floor => -- Right half of floor.
         Output (Output_Object, Wide_Character'Val (16#230b#));
      when ARM_Output.Thin_Space => -- A thinner than usual space.
         Output (Output_Object, Wide_Character'Val (16#2009#));
      when ARM_Output.Left_Quote => -- A left facing single quote.
         Output (Output_Object, Wide_Character'Val (16#2018#));
      when ARM_Output.Right_Quote => -- A right facing single quote.
         Output (Output_Object, Wide_Character'Val (16#2019#));
      when ARM_Output.Left_Double_Quote => -- A left facing double quote.
         Output (Output_Object, Wide_Character'Val (16#201c#));
      when ARM_Output.Right_Double_Quote => -- A right facing double quote.
         Output (Output_Object, Wide_Character'Val (16#201d#));
      when ARM_Output.Small_Dotless_I =>  -- A small i without a dot
                                          -- (Unicode(16#0131#).
         Output (Output_Object, Wide_Character'Val (16#0131#));
      when ARM_Output.Capital_Dotted_I => -- A large I with a dot
                                          -- (Unicode(16#0130#).
         Output (Output_Object, Wide_Character'Val (16#0130#));
      end case;
   end Special_Character;

   -----------------------
   -- Unicode_Character --
   -----------------------

   procedure Unicode_Character
     (Output_Object : in out XML_Output_Type;
      Char          : in ARM_Output.Unicode_Type)
   is
   begin
      Is_Valid (Output_Object);

      Output (Output_Object, Wide_Character'Val (Char));
   end Unicode_Character;

   -------------------
   -- End_Hang_Item --
   -------------------

   procedure End_Hang_Item (Output_Object : in out XML_Output_Type) is
   begin
      -- TODO : don't know what to do here. (?)
      null;
   end End_Hang_Item;

   -----------------
   -- Text_Format --
   -----------------

   procedure Text_Format
     (Output_Object : in out XML_Output_Type;
      Format        : in ARM_Output.Format_Type)
   is
      Current_Format : ARM_Output.Format_Type := Output_Object.Current_Format;

      procedure Open_Format_Tags
        (Output_Object : in out XML_Output_Type;
         Format        : in ARM_Output.Format_Type;
         From          : in Format_Tags)
      is
      -- Writes the format tags to the output file in the correct order.
      -- The 'From' parameter designates wich tag it should start with.
      begin
         if From <= Location then
            case Format.Location is
               when ARM_Output.Normal =>
                  Output (Output_Object, "<location type=""normal"">");
               when ARM_Output.Subscript =>
                  Output (Output_Object, "<location type=""subscript"">");
               when ARM_Output.Superscript =>
                  Output (Output_Object, "<location type=""superscript"">");
            end case;
            Output_Object.Format.Append (Location);
         end if;

         if From <= Added_Version then
            declare
               use type ARM_Contents.Change_Version_Type;
            begin
               if Format.Added_Version /= '0' then
                  Output
                    (Output_Object,
                     "<added_version type=""" &
                     WS
                        (ARM_Contents.Change_Version_Type'Image
                            (Format.Added_Version)) &
                     """>");
                  Output_Object.Format.Append (Added_Version);
               end if;
            end;
         end if;

         if From <= Version then
            declare
               use type ARM_Contents.Change_Version_Type;
            begin
               if Format.Version /= '0' then
                  Output
                    (Output_Object,
                     "<version type=""" &
                     WS
                        (ARM_Contents.Change_Version_Type'Image
                            (Format.Version)) &
                     """>");
                  Output_Object.Format.Append (Version);
               end if;
            end;
         end if;

         if From <= Change then
            case Format.Change is
               when ARM_Output.None =>
                  null; -- Default needs no tag.
               when ARM_Output.Insertion =>
                  Output (Output_Object, "<change type=""insertion"">");
                  Output_Object.Format.Append (Change);
               when ARM_Output.Deletion =>
                  Output (Output_Object, "<change type=""deletion"">");
                  Output_Object.Format.Append (Change);
               when ARM_Output.Both =>
                  Output (Output_Object, "<change type=""both"">");
                  Output_Object.Format.Append (Change);
            end case;
         end if;

         if From <= Color then
            case Format.Color is
               when ARM_Output.Default =>
                  -- Default does not need a tag.
                  null;
               when ARM_Output.Black =>
                  Output (Output_Object, "<color shade=""black"">");
                  Output_Object.Format.Append (Color);
               when ARM_Output.Red =>
                  Output (Output_Object, "<color shade=""red"">");
                  Output_Object.Format.Append (Color);
               when ARM_Output.Green =>
                  Output (Output_Object, "<color shade=""green"">");
                  Output_Object.Format.Append (Color);
               when ARM_Output.Blue =>
                  Output (Output_Object, "<color shade=""blue"">");
                  Output_Object.Format.Append (Color);
            end case;
         end if;

         if From <= Size then
            declare
               Attr_Size : String := ARM_Output.Size_Type'Image (Format.Size);
               use type ARM_Output.Size_Type;
            begin
               if Format.Size /= 0 then

                  Output
                    (Output_Object,
                     "<size value=""" & WS (Attr_Size) & """>");

                  Output_Object.Format.Append (Size);
               end if;
            end;
         end if;

         if From <= Font then
            case Format.Font is
               when ARM_Output.Roman =>
                  -- serif
                  Output (Output_Object, "<font type=""roman"">");
                  Output_Object.Format.Append (Font);
               when ARM_Output.Swiss =>
                  -- sans
                  Output (Output_Object, "<font type=""swiss"">");
                  Output_Object.Format.Append (Font);
               when ARM_Output.Fixed =>
                  -- fixed width
                  Output (Output_Object, "<font type=""fixed"">");
                  Output_Object.Format.Append (Font);
               when ARM_Output.Default =>
                  -- Default font does not need a tag.
                  null;
            end case;
         end if;

         if From <= Italic then
            -- If italic is set, open the tag.
            if Format.Italic then
               Output (Output_Object, "<italic>");
               Output_Object.Format.Append (Italic);
            end if;
         end if;

         if From <= Bold then
            -- If italic is set, open the tag.
            if Format.Bold then
               Output (Output_Object, "<bold>");
               Output_Object.Format.Append (Bold);
            end if;
         end if;

      end Open_Format_Tags;

      use type ARM_Output.Location_Type;
      use type ARM_Output.Change_Type;
      use type ARM_Output.Color_Type;
      use type ARM_Output.Size_Type;
      use type ARM_Output.Font_Family_Type;

      Tag_Changed : Format_Tags;

   begin
      -- Determine the first tag that has changed and close all sub-tags
      --including the changed tag.
      if Current_Format.Location /= Format.Location then
         Close_All_Format_Tags (Output_Object);
         Tag_Changed := Location;
      elsif Current_Format.Added_Version /= Format.Added_Version then
         Close_Format_Until
           (Output_Object,
            Format_Tags'Pred (ARM_XML.Added_Version));
         Tag_Changed := Added_Version;
      elsif Current_Format.Version /= Format.Version then
         Close_Format_Until
           (Output_Object,
            Format_Tags'Pred (ARM_XML.Version));
         Tag_Changed := Version;
      elsif Current_Format.Change /= Format.Change then
         Close_Format_Until
           (Output_Object,
            Format_Tags'Pred (ARM_XML.Change));
         Tag_Changed := Change;
      elsif Current_Format.Color /= Format.Color then
         Close_Format_Until (Output_Object, Format_Tags'Pred (ARM_XML.Color));
         Tag_Changed := Color;
      elsif Current_Format.Size /= Format.Size then
         Close_Format_Until (Output_Object, Format_Tags'Pred (ARM_XML.Size));
         Tag_Changed := Size;
      elsif Current_Format.Font /= Format.Font then
         Close_Format_Until (Output_Object, Format_Tags'Pred (ARM_XML.Font));
         Tag_Changed := Font;
      elsif Current_Format.Italic /= Format.Italic then
         Close_Format_Until
           (Output_Object,
            Format_Tags'Pred (ARM_XML.Italic));
         Tag_Changed := Italic;
      elsif Current_Format.Bold /= Format.Bold then
         Close_Format_Until (Output_Object, Format_Tags'Pred (ARM_XML.Bold));
         Tag_Changed := Bold;
      end if;

      Open_Format_Tags (Output_Object, Format, Tag_Changed);
   end Text_Format;

   ----------------------
   -- Clause_Reference --
   ----------------------

   procedure Clause_Reference
     (Output_Object : in out XML_Output_Type;
      Text          : in String;
      Clause_Number : in String)
   is
   begin
      Is_Valid (Output_Object);
      Output
        (Output_Object,
         "<clause_reference number=""" &
         WS (Clause_Number) &
         """>" &
         WS (Text) &
         "</clause_reference>");
   end Clause_Reference;

   ------------------
   -- Index_Target --
   ------------------

   procedure Index_Target
     (Output_Object : in out XML_Output_Type;
      Index_Key     : in Natural)
   is
   begin
      Is_Valid (Output_Object);

      declare
         use Ada.Strings;
         Key_Attr : String := Fixed.Trim (Natural'Image (Index_Key), Left);
      begin
         Output
           (Output_Object,
            "<index_target key=""" & WS (Key_Attr) & """ />");
      end;
   end Index_Target;

   ---------------------
   -- Index_Reference --
   ---------------------

   procedure Index_Reference
     (Output_Object : in out XML_Output_Type;
      Text          : in String;
      Index_Key     : in Natural;
      Clause_Number : in String)
   is
   begin
      Is_Valid (Output_Object);
      Output
        (Output_Object,
         "<index_reference key=""" &
         WS (Natural'Image (Index_Key)) &
         " number=""" &
         WS (Clause_Number) &
         WS (Text) &
         "</index_reference>");
   end Index_Reference;

   ------------------
   -- DR_Reference --
   ------------------

   procedure DR_Reference -- DR means defect report.
     (Output_Object : in out XML_Output_Type;
      Text          : in String;
      DR_Number     : in String)
   is
   begin
      Is_Valid (Output_Object);

      Output
        (Output_Object,
         "<dr_reference dr_number=""" &
         WS (DR_Number) &
         """>" &
         WS (Text) &
         "</dr_reference>");
   end DR_Reference;

   ------------------
   -- AI_Reference --
   ------------------

   procedure AI_Reference
     (Output_Object : in out XML_Output_Type;
      Text          : in String;
      AI_Number     : in String)
   is
   begin
      Is_Valid (Output_Object);
      -- References the AI documents on the ada-auth.org server.
      Output
        (Output_Object,
         "<ai_reference url=""http://www.ada-auth.org/cgi-bin/cvsweb.cgi/AIs/"
         &
         WS (AI_Number) &
         ".TXT"">" &
         WS (Text) &
         "</ai_reference>");
   end AI_Reference;

   ------------------
   -- Local_Target --
   ------------------

   procedure Local_Target
     (Output_Object : in out XML_Output_Type;
      Text          : in String;
      Target        : in String)
   is
   begin
      Is_Valid (Output_Object);
      Output
        (Output_Object,
         "<local_target target=""" & WS (Target) & """>");
      Ordinary_Text (Output_Object, Text);
      Output (Output_Object, "</local_target>");
   end Local_Target;

   ----------------
   -- Local_Link --
   ----------------

   procedure Local_Link
     (Output_Object : in out XML_Output_Type;
      Text          : in String;
      Target        : in String;
      Clause_Number : in String)
   is
   begin
      Is_Valid (Output_Object);

      Local_Link_Start (Output_Object, Target, Clause_Number);
      Ordinary_Text (Output_Object, Text);
      Local_Link_End (Output_Object, Target, Clause_Number);
   end Local_Link;

   ----------------------
   -- Local_Link_Start --
   ----------------------

   procedure Local_Link_Start
     (Output_Object : in out XML_Output_Type;
      Target        : in String;
      Clause_Number : in String)
   is
   begin
      Is_Valid (Output_Object);
      Output
        (Output_Object,
         "<local_link target=""" &
         WS (Target) &
         """ clause_number=""" &
         WS (Clause_Number) &
         """>");
   end Local_Link_Start;

   --------------------
   -- Local_Link_End --
   --------------------

   procedure Local_Link_End
     (Output_Object : in out XML_Output_Type;
      Target        : in String;
      Clause_Number : in String)
   is
   begin
      Is_Valid (Output_Object);
      Output (Output_Object, "</local_link>");
   end Local_Link_End;

   --------------
   -- URL_Link --
   --------------

   procedure URL_Link
     (Output_Object : in out XML_Output_Type;
      Text          : in String;
      URL           : in String)
   is
   begin
      Is_Valid (Output_Object);
      Output
        (Output_Object,
         "<url_link url=""" & WS (URL) & """>" & WS (Text) & "</url_link>");
   end URL_Link;

   -------------
   -- Picture --
   -------------

   procedure Picture
     (Output_Object : in out XML_Output_Type;
      Name          : in String;
      Descr         : in String;
      Alignment     : in ARM_Output.Picture_Alignment;
      Height, Width : in Natural;
      Border        : in ARM_Output.Border_Kind)
   is
   begin
      Is_Valid (Output_Object);
      declare
         Attr_Alignment : String :=
            ARM_Output.Picture_Alignment'Image (Alignment);
         Attr_Height    : String := Natural'Image (Height);
         Attr_Width     : String := Natural'Image (Width);
         Attr_Border    : String := ARM_Output.Border_Kind'Image (Border);
      begin
         Output
           (Output_Object,
            "<picture name=""" &
            WS (Name) &
            """ descr=""" &
            WS (Descr) &
            """ alignment=""" &
            WS (Attr_Alignment) &
            """ height=""" &
            WS (Attr_Height) &
            """ width=""" &
            WS (Attr_Width) &
            """ border=""" &
            WS (Attr_Border) &
            """ />");
      end;
   end Picture;

end ARM_XML;
