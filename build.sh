#!/usr/bin/env sh

rm -rf output/*
gnatmake -P formatter.gpr
cp bin/Arm_Form rm/Arm_Form
cd rm
./Arm_Form AARM xml Show-Changes 2
cd ../output
